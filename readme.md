## AIFap Recservice

### Setup instructions:

- Replace S3 keys in dataset.py:67
- Replace redis details in dataset.py:111
- Create a new python environment using pyenv/pipenv/virtualenv. Python version should be 3.7.1
- Install pip dependancies from requirements.txt

### Training:

Data exported by the webserver should go into onto the S3 bucket specified in dataset.py.
dataset.py will pull down the data from S3 and store it in `data/` together with the trained models.

Add some kind of cron job to run train.py:

```
30 * * * *      cd /path/to/recservice && /path/to/pyenv/bin/python /path/to/recservice/train.py
```

### Running the server:

Use some kind of WSGI wrapper to run flask. We used gunicorn. Here's the systemd unit we
used to run it:

```
[Unit]
Description=uWSGI instance to serve myproject
After=network.target

[Service]
User=REPLACEME
Group=REPLACEME
WorkingDirectory=/path/to/recservice
Environment="PATH=/path/to/pyenv/bin"
ExecStart=/path/to/pyenv/bin/gunicorn --workers 1 --bind 0.0.0.0:8080 --log-file /var/log/uwsgi/app.log --timeout 1200 --backlog 2 wsgi
Restart=always
RuntimeMaxSec=10800

[Install]
WantedBy=multi-user.target
```

### Usage:

When the server is running it provides an endpoint at `/recommend/:user_id` which
returns recommendations. 

### Notes:

This code is from just after we switched the view tracking to redis. To switch back to using the CSV exports for view tracking, look at the comments in dataset.py

Also, this code is just a mess in general. It's one of dozens of versions that all had slightly different
problems. This version of contentnewv1 and collabuserv3 both use lightfm, which is the FM library we 
had the most luck with.