import logging
import sys
import pandas

from flask import Flask
from flask import jsonify

from dataset import Dataset
from recommender import Recommender

DATASET_PATH = './data/dataset.hdf5'
MODEL_PATH = './data/models.pickle'

# Set up logging
logging.basicConfig(level=logging.INFO)

# Set up flask
app = Flask(__name__)

gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.DEBUG)
app.logger.debug('Starting app...')


app.logger.debug('Loading dataset')
# Load data
data = Dataset()
try:
    data.load(DATASET_PATH)
except Exception as e:
    app.logger.exception('Couldn\'t load dataset, exiting.')
    sys.exit(1);
    
app.logger.debug('Loading models')
recommender = Recommender()
try:
    recommender.load(MODEL_PATH)
except Exception as e:
    app.logger.exception('Couln\'t load recommender, exiting.')
    sys.exit(1);
    
#recommender.train(data)


# Flask app
@app.route('/')
def index():
    return 'AIF Rec System V4'

@app.route('/recommend/<int:external_user_id>')
def recommend(external_user_id):
    recs = recommender.recommend(data, external_user_id)
    
    return jsonify({
        'success': True,
        'user_id': external_user_id,
        'recommendations': recs
    })

@app.route('/test/recommend/<int:external_user_id>')
def rectest(external_user_id):
    recs = recommender.recommend(data, external_user_id)
    
    posturldata = pandas.read_csv('./posturldata3.csv', names=['post_id', 'type_gender', 'src'], delimiter=',', quotechar='"', escapechar='\\', encoding='utf8')
    posturldata.set_index('post_id', inplace=True)
    
    html = '<div>'
    
    for rec in recs[:100]:
        if posturldata.index.contains(rec['post_id']):
            src = posturldata.loc[rec['post_id']].src
            if '.mp4' in src or '.webm' in src:
                src = '<video src="' + src + '" style="height:150px"></video>'
            else:
                src = '<img src="' + src + '" style="height:150px">'
        else:
            src = ''
        
        html += '<div>'
        html += '<strong>' + rec['strategy'] + ' ~> ' + str(rec['post_id']) + ': ' + str(rec['score']) + '</strong>'
        html += '<br>' + src
        html += '</div>'
    
    html += '</div>'
    
    return html
    
app.logger.debug('Starting Flask')