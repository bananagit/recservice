import logging
import sys

from dataset import Dataset
from recommender import Recommender

DATASET_PATH = './data/dataset.hdf5'
MODEL_PATH = './data/models.pickle'

# Set up logging
logging.basicConfig(level=logging.INFO)

print('Loading data CSVs...')
# Load new data
data = Dataset()
try:
    data.download_csvs()
    data.load_csvs()
except Exception as e:
    print(e)
    print('Couldn\'t load data CSVs, exiting.')
    sys.exit(1);
print('Loading data complete')

print('Training models')
# Train recs
recommender = Recommender()
try:
    recommender.train(data)
except Exception as e:
    print(e)
    print('Couln\'t train recommeder, exiting.')
    sys.exit(1);
print('Training complete')
    

print('Saving output')
# Save results
data.save(DATASET_PATH)
recommender.save(MODEL_PATH)
print('Model output complete')
