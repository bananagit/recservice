import logging
import pickle
import math
import random

from strats.contentnewv1 import ContentNewV1
from strats.collabuserv3 import CollabUserV3

log = logging.getLogger('aif.recsystem.recommender')

# Scoring
strat_info = {
    'CollabUserV3': {
        'weight': 1,
        'noise': 0
    },
    'ContentNewV1': {
        'weight': 0.3,
        'noise': 0,
    }
}
score_scale = 1000000

class Recommender:
    def __init__(self):
        self.models = [
        ]
        
    def train(self, dataset):
        self.models = [
            ContentNewV1(),
            CollabUserV3()
        ]
        
        for model in self.models:
            model.train(dataset)
            
    def recommend(self, dataset, external_user_id):
        log.info('Recommending for %s' % external_user_id)
        recs = []
        for model in self.models:
            strat_recs = model.recommend(dataset, external_user_id)
            self.normalize_recs_for_strat(strat_recs)
            recs = recs + strat_recs
            log.info('Got %s recs from %s' % (len(recs), model))
                
        log.info('Sorting recs')
        recs = sorted(recs, key=lambda r: r['score'], reverse=True)
        log.info('Returning recs')
        return recs
    
    def recommend_anonymous(self, dataset, profile):
        recs = []
        for model in self.models:
            strat_recs = model.recommend_anonymous(dataset, features)
            self.normalize_recs_for_strat(strat_recs)
            recs = recs + strat_recs
                
        recs = sorted(recs, key=lambda r: r['score'], reverse=True)
        return recs
    
    def normalize_recs_for_strat(self, recs):
        scores = [r['score'] for r in recs]
        if len(scores) < 1:
            return []
        
        score_min = min(scores)
        score_max = max(scores)
        
        stratInfo = strat_info[recs[0]['strategy']]
        
        def gauss(mean, sd):
            x = random.random()
            y = random.random()
            return math.sqrt(-2*math.log(x)) * math.cos(2*math.pi*y)*sd + mean;
        
        for r in recs:
            normal_score = 1
            if (score_max - score_min) > 0.000001:
                normal_score = (r['score'] - score_min) / (score_max - score_min)
            weighted_score = normal_score * stratInfo['weight']
            if stratInfo['noise'] > 0:
                weighted_score *= gauss(1, stratInfo['noise'])
            score = weighted_score * score_scale
            if math.isnan(score):
                score = 0
            r['score'] = int(score)
        
    def save(self, filename):
        store = {
            'models': self.models,
        }
        with open(filename, 'wb') as f:
            pickle.dump(store, f)
        
    def load(self, filename):
        with open(filename, 'rb') as f:
            store = pickle.load(f)
            self.models = store['models']