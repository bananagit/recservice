import logging
import math
import numpy as np
from scipy.special import expit

from .recstrategy import RecStrategy

log = logging.getLogger('aif.recsystem.strategies.contentnew1')

class ContentNewV1(RecStrategy):
    def train(self, dataset):
        pass
    
    def recommend(self, dataset, external_user_id):
        log.info('Recommending for #%s' % external_user_id)
        unviewed_posts = dataset.unviewed_posts(external_user_id)
        profile = dataset.generate_profile(external_user_id)
        
        log.info('Copying posts')
        scored_posts = unviewed_posts
        log.info('Scoring %s posts' % len(scored_posts))
        
        cache = None
        if external_user_id in dataset.content_score_cache:
            log.info('Loading content scores from cache')
            cache = dataset.content_score_cache[external_user_id]
        else:
            log.info('No content scores in cache')
            dataset.content_score_cache[external_user_id] = {}
    
        # This is faster than using df.apply for some reason
        rec_scores = []
        for post in scored_posts.itertuples():
            if cache is not None and post.post_id in cache:
                rec_scores.append(cache[post.post_id])
                continue
                
            score = 0
            score += profile['subreddits'][post.subreddit_id]
            score += profile['types']['gender_type'][post.type_gender]
            score += profile['types']['artstyle_type'][post.type_artstyle]
            score += profile['types']['media_type'][post.type_media]
            
            tag_score = 0
            tag_scores = []
            for tag_id in post.tags:
                if tag_id in profile['tags']:
                    tag_scores.append(profile['tags'][tag_id])
            if len(tag_scores) > 0:
                tag_score = sum(tag_scores) / len(tag_scores)
                
            score += tag_score

            #return np.sign(score) * np.log(1 + abs(score) / np.power(10, 0.01))
            #return expit(score)
            #rec_scores.append(get_score(post))
            rec_scores.append(score)
            dataset.content_score_cache[external_user_id][post.post_id] = score
            
        scored_posts['rec_score'] = rec_scores
        
        top = scored_posts.nlargest(1000, 'rec_score')
        
        return [
            self.makeResult(x.post_id, x.rec_score, 'ContentNewV1') for x in top.itertuples()
        ]
        
    def can_recommend_by_id(self):
        return True
        
    def can_recommend_by_profile(self):
        return False