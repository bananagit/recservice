import logging
import math
import numpy as np
import pandas
import random
from scipy.special import expit

from lightfm import LightFM
from lightfm.data import Dataset as LightFMDataset
from lightfm.cross_validation import random_train_test_split

from scipy import sparse

from .recstrategy import RecStrategy

log = logging.getLogger('aif.recsystem.strategies.collabuserv3')

# Model parameters
epochs = 20 
threads = 1
hyperparams = {
    'no_components': 10, #58, 
    'learning_schedule': 'adagrad', 
    'loss': 'warp', 
    'learning_rate': 0.05, #0.004954883805560784, 
    'max_sampled': 10,
}

# Feature mapping
orient_map = {
    'solo-men': 'gay',
    'solo-women': 'lesbian',
    'gay': 'gay',
    'lesbian': 'lesbian',
    'straight': 'straight',
    'transexual': 'transexual',
    'unknown': 'unknown',
}
solo_map = {
    'solo-men': 'solo',
    'solo-women': 'solo',
    'gay': 'group',
    'lesbian': 'group',
    'straight': 'group',
    'transexual': 'group',
    'unknown': 'unknown',
}

def get_lightfm_post_features(post):
    features = {}
    features['id:' + str(post.post_id)] = 1

    if post.type_gender != 'unknown':
       features['type_gender:' + post.type_gender] = 0.3
       features['artstyle_orientation:' + post.type_artstyle + ':' + orient_map[post.type_gender]] = 0.6

    features['type_artstyle:' + post.type_artstyle] = 0.3
    features['type_media:' + post.type_media] = 0.2
    features['subreddit_id:' + str(post.subreddit_id)] = 0.3

    for tag in post.tags:
        features['tag:' + tag] = 0.3

    return features

def get_lightfm_user_features(user):
    features = {}
    features['id' + str(user.user_id)] = 1
    
    if user.gender_filter and isinstance(user.gender_filter, dict):
        gender_filter = user.gender_filter
        for gender in gender_filter:
            features['gender_' + gender] = 1
    if user.media_filter and isinstance(user.media_filter, dict):
        media_filter = user.media_filter
        for media in media_filter:
            features['media_' + media] = 1
    if user.artstyle_filter and isinstance(user.artstyle_filter, dict):
        artstyle_filter = user.artstyle_filter
        for artstyle in artstyle_filter:
            features['artstyle_' + artstyle] = 1

    return features


class CollabUserV3(RecStrategy):
    def __init__(self):
        self.lightfm_model = None
        self.lightfm_item_features = None
        self.lightfm_user_features = None
        
        self.uid_map = None
        self.uf_map = None
        self.iid_map = None
        self.if_map = None
        
        self.inv_uid_map = None
        self.inv_iid_map = None
        
        #self.testset = None
        self.trainset = None
        
        
    def train(self, dataset):
        log.info('Training...')
        log.info('Training: generating features')
        # Generate item/user features
        all_item_features = [(p.post_id, get_lightfm_post_features(p)) for p in dataset.posts.itertuples()]
        all_user_features = [(u.user_id, get_lightfm_user_features(u)) for u in dataset.users.itertuples()]
        
        # Get unique set of item/user features
        item_features = set()
        user_features = set()
        for _, features in all_item_features:
            item_features.update(features)
        for _, features in all_user_features:
            user_features.update(features)
            
        log.info('Training: fitting lightfm data')
        # Fit dataset
        lightfm_dataset = LightFMDataset()
        lightfm_dataset.fit(
            dataset.users['user_id'].values,
            dataset.posts['post_id'].values,
            item_features=item_features,
            user_features=user_features)
        
        log.info('Training: building interactions matrix')
        # Build interactions matrix
        all_ratings = set((r.user_id, r.post_id) for r in dataset.ratings.itertuples())
        (interactions, weights) = lightfm_dataset.build_interactions(all_ratings)
        
        log.info('Training: building features matrix')
        # Build feature matrices
        lightfm_item_features = lightfm_dataset.build_item_features(all_item_features)
        lightfm_user_features = lightfm_dataset.build_user_features(all_user_features)
        self.lightfm_item_features = lightfm_item_features
        self.lightfm_user_features = lightfm_user_features
        
        log.info('Training: retrieving mappings')
        # Get mappings
        (uid_map, uf_map, iid_map, if_map) = lightfm_dataset.mapping()
        self.uid_map = uid_map
        self.uf_map = uf_map
        self.iid_map = iid_map
        self.if_map = if_map
        
        log.info('Training: generating inverse mappings')
        # Get inverse mappings
        inv_uid_map = {v: k for k, v in uid_map.items()}
        inv_iid_map = {v: k for k, v in iid_map.items()}
        
        self.inv_uid_map = inv_uid_map
        self.inv_iid_map = inv_iid_map
        
        log.info('Training: creating train/test split')
        # Get interactions split
        (trainset, testset) = random_train_test_split(interactions, 0.2)
        self.trainset = trainset
        #self.testset = testset
        
        log.info('Training: train lightfm model')
        self.lightfm_model = LightFM(**hyperparams)
        self.lightfm_model.fit(
            self.trainset,
            epochs=epochs,
            item_features=self.lightfm_item_features,
            num_threads=threads)
        log.info('Training complete')
    
    def recommend(self, dataset, external_user_id):
        log.info('Rec: Recommending for #%s' % external_user_id);
        
        log.info('Rec: Getting unviewed');
        unviewed_posts = dataset.unviewed_posts(external_user_id)
        
        log.info('Rec: Generating profile');
        profile = dataset.generate_profile(external_user_id)
        
        # Get collab scores for unviewed items
        log.info('Rec: Predicting lightfm scores');
        lightfm_scores = self.get_lightfm_scores(external_user_id, unviewed_posts)
        log.info('Rec: Got lightfm scores');
        
        log.info('Rec: Getting content scores for %s items' % len(lightfm_scores));
        cache = None
        if external_user_id in dataset.content_score_cache:
            log.info('Loading content scores from cache')
            cache = dataset.content_score_cache[external_user_id]
        else:
            log.info('No content scores in cache')
            dataset.content_score_cache[external_user_id] = {}
            
        # Calc content scores
        content_scores = {}
        posts_by_id = unviewed_posts.set_index('post_id')
        for external_post_id, _ in lightfm_scores:
            if cache is not None and external_post_id in cache:
                content_scores[external_post_id] = cache[external_post_id]
                continue
            
            
            post = posts_by_id.loc[external_post_id]
            
            score = 0
            if post.subreddit_id in profile['subreddits']:
                score += profile['subreddits'][post.subreddit_id]
            if post.type_gender in profile['types']['gender_type']:
                score += profile['types']['gender_type'][post.type_gender]
            if post.type_artstyle in profile['types']['artstyle_type']:
                score += profile['types']['artstyle_type'][post.type_artstyle]
            if post.type_media in profile['types']['media_type']:
                score += profile['types']['media_type'][post.type_media]
    
            tag_score = 0
            tag_scores = []
            for tag_id in post.tags:
                if tag_id in profile['tags']:
                    tag_scores.append(profile['tags'][tag_id])
            if len(tag_scores) > 0:
                tag_score = sum(tag_scores) / len(tag_scores)
    
            score += tag_score
    
#            return expit(score)
            content_scores[external_post_id] = score
            dataset.content_score_cache[external_user_id][external_post_id] = score
            
        log.info('Rec: Got content scores (%s)' % len(content_scores));
            
        log.info('Rec: Getting score ranges');
        # Get score ranges
        content_score_min = min(content_scores.values())
        content_score_max = max(content_scores.values())

        lightfm_score_min = min([x[1] for x in lightfm_scores])
        lightfm_score_max = max([x[1] for x in lightfm_scores])
        log.info('Rec: Got score ranges');
        
        recs = []
        
        # Calculate final scores
        for external_post_id, lightfm_score in lightfm_scores:
            content_score = content_scores[external_post_id]

            if content_score_min == content_score_max:
                content_score = 1
            else:
                content_score = (content_score - content_score_min) / (content_score_max - content_score_min)

            if lightfm_score_min == lightfm_score_max:
                lightfm_score = 1
            else:
                lightfm_score = (lightfm_score - lightfm_score_min) / (lightfm_score_max - lightfm_score_min)

            if content_score > 0.5:
                content_score = min(content_score, 0.9) + random.uniform(0, 0.2)
                lightfm_score = min(lightfm_score, 0.6) + random.uniform(0, 0.1)
                final_score = (1 * lightfm_score) + (1 * content_score)
                recs.append(self.makeResult(external_post_id, final_score, 'CollabUserV3', None))


        log.info('Got recs');
        sorted_recs = sorted(recs, key=lambda r: r['score'], reverse=True)
        top_n = sorted_recs[:1000]
        log.info('returning recs');
        return top_n


    def get_lightfm_scores(self, external_user_id, posts):
        internal_user_id = self.uid_map[external_user_id]
        
        internal_item_ids = set(self.iid_map[epid] for epid in posts['post_id'])
        all_internal_item_ids = list(self.iid_map.values())
        
        log.info('Predicting..');
        lfm_scores = self.lightfm_model.predict(
            internal_user_id,
            all_internal_item_ids,
            item_features=self.lightfm_item_features
        )
        
        log.info('Processing scores..');
        item_scores = {self.inv_iid_map[k]: v for k, v in enumerate(lfm_scores) if k in internal_item_ids}
        
        log.info('Sorting scores..');
        item_scores_sorted = sorted(item_scores.items(), key=lambda e: e[1], reverse=True)
        
        item_scores_sorted = item_scores_sorted[:50000]
        return item_scores_sorted
    
    def get_content_score(self, profile, post):
        score = 0
        if post.subreddit_id in profile['subreddits']:
            score += profile['subreddits'][post.subreddit_id]
        if post.type_gender in profile['types']['gender_type']:
            score += profile['types']['gender_type'][post.type_gender]
        if post.type_artstyle in profile['types']['artstyle_type']:
            score += profile['types']['artstyle_type'][post.type_artstyle]
        if post.type_media in profile['types']['media_type']:
            score += profile['types']['media_type'][post.type_media]

        tag_score = 0
        tag_scores = []
        for tag_id in post.tags:
            if tag_id in profile['tags']:
                tag_scores.append(profile['tags'][tag_id])
        if len(tag_scores) > 0:
            tag_score = sum(tag_scores) / len(tag_scores)

        score += tag_score

        #return np.sign(score) * np.log(1 + abs(score) / np.power(10, 0.01))
        return expit(score)

